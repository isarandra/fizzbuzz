import java.util.Scanner;

public class FizzBuzz {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        fizzBuzz(takeInput());
    }

    private static int takeInput() {
        System.out.print("Masukkan angka: ");
        int input = scanner.nextInt();
        return input;
    }

    private static void fizzBuzz(int angka) {
        for (int i =1;i<=angka;i++){
            if(i%3==0 && i%5==0){
                System.out.print("FizzBuzz ");
            }
            else if(i%3==0){
                System.out.print("Fizz ");
            }
            else if(i%5==0){
                System.out.print("Buzz ");
            }
            else{
                System.out.print(i + " ");
            }
        }
    }
}

